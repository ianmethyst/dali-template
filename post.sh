#!/bin/bash

echo "Linking dotfiles dir"
ln -s $HOME/.config/dali/out $HOME/.dotfiles

echo "Linking individual dotfiles"
cd $HOME/.dotfiles
for dir in $(find . -maxdepth 1 -type d -printf "%f\n" | grep -v '\.'); do 
  stow -d $HOME/.dotfiles -t $HOME $dir
done

echo "Loading X settings"
ln -s $HOME/.Xdefaults $HOME/.Xresources
xrdb $HOME/.Xdefaults

echo "Reloading i3"
i3-msg restart &
