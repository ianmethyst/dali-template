#!/bin/bash

echo "Unlinking individual dotfiles"
if [ -L "$HOME/.Xresources" ]; then
unlink $HOME/.Xresources
fi

if [ -d "$HOME/.dotfiles" ]; then 
  OLDIR=$PWD

  cd $HOME/.dotfiles
  for dir in $(find . -maxdepth 1 -type d -printf "%f\n" | grep -v '\.'); do 
    stow -D -d $HOME/.dotfiles -t $HOME $dir
  done

  cd $OLDIR
fi

echo "Unlinking dotfiles dir"
if [ -L "$HOME/.dotfiles" ]; then
  unlink $HOME/.dotfiles 
fi

echo "Cleaning out dir"
rm -rf $HOME/.config/dali/out/*
