" ########
" # Plug #
" ########

call plug#begin('~/.local/share/nvim/plugged')

Plug 'chriskempson/base16-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'altercation/vim-colors-solarized'
Plug 'arcticicestudio/nord-vim'

Plug 'itchyny/lightline.vim'
Plug 'daviesjamie/vim-base16-lightline'
Plug 'maximbaz/lightline-ale'

Plug 'gioele/vim-autoswap' "Requires wmctrl

Plug 'sheerun/vim-polyglot'
Plug 'sirtaj/vim-openscad'
Plug 'othree/yajs.vim'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'jparise/vim-graphql'
Plug 'styled-components/vim-styled-components'

Plug 'vim-pandoc/vim-pandoc-syntax'

Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'
Plug 'xuyuanp/nerdtree-git-plugin'

Plug 'airblade/vim-gitgutter'

Plug 'nathanaelkane/vim-indent-guides'

" Plug 'donRaphaco/neotex', { 'for': 'tex' }
Plug 'lervag/vimtex'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'w0rp/ale'

call plug#end()

" ###########
" # General #
" ###########

let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python'

" Syntax
filetype plugin indent on
let g:polyglot_disabled = ['js', 'latex']
syntax on

" Colorscheme
let base16colorspace=256
let g:dracula_colorterm=0
let g:nord_underline = 1
let g:nord_italic = 1
let g:nord_italic_comments = 0
let g:nord_cursor_line_number_background = 1
let g:solarized_termcolors= 16 
let g:solarized_bold = 1 
let g:solarized_underline = 1 
let g:solarized_italic = 1

colorscheme {{VIMCOLORSCHEME}}
set bg={{VIMBACKGROUND}}

" make the highlighting of tabs and other non-text less annoying
highlight SpecialKey ctermbg=none ctermfg=8
highlight NonText ctermbg=none ctermfg=8

" Tabs and Identation
set expandtab
set copyindent
set preserveindent
set softtabstop=2
set shiftwidth=2
set tabstop=2

" set list
" set lcs+=space:·

" Essentials
set encoding=utf-8                              " Set encoding
set ffs=unix,dos,mac
set title titlestring=                          " Change the terminal's title
set number                                      " Show line numbers
set relativenumber                              " Show relative numbers
set cursorline                                  " Highlight current line
set showcmd                                     " Show command in bottom bar
set wildmenu                                    " Visual autocomplete for command menu
set wildignore+=*/tmp/*,*.so,*.swp,*.zip        " MacOSX/Linux
"set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe    " Windows
set timeoutlen=1000 ttimeoutlen=0               " No timeout before keypresses
set scrolloff=10
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes
set lazyredraw          " Redraw only when we need to.
set showmatch           " Highlight matching [{()}]

" History
set history=1000        " Remember more commands and search history
set undolevels=1000     " Use many muchos levels of undo

" Search
set incsearch           " Search as characters are entered
set hlsearch            " Highlight matches
set ignorecase          " Ignore case when searching
set smartcase           " When searching try to be smart about cases

" Folding
set foldenable          " Enable folding
set foldlevelstart=99   " Don't fold things by default
set foldcolumn=0        " Don't show fold column
set foldmethod=indent
set foldopen=block,hor,insert,jump,mark,percent,quickfix,search,tag,undo

" Mouse support
set mouse=a

" Backups
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" ###########
" # Keymaps #
" ###########

" Leader
let mapleader=","       

" Movement
nnoremap j gj
nnoremap k gk

" Turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" Session save
nnoremap <leader>s :mksession<CR>

" :W sudo saves the file
command W w !sudo tee % > /dev/null

" NERD Tree
map <C-n> :NERDTreeToggle<CR>

" Ale
nmap <silent> <leader>aj :ALENext<cr>
nmap <silent> <leader>ak :ALEPrevious<cr>

" fzf
nnoremap <c-p> :Files<cr>

" #############
" # Statusbar #
" #############

set laststatus=2
set noshowmode

let g:lightline = {
      \ 'colorscheme': '{{VIMLIGHTLINE}}',
      \ 'component_expand': {
      \      'linter_checking': 'lightline#ale#checking',
      \      'linter_warnings': 'lightline#ale#warnings',
      \      'linter_errors': 'lightline#ale#errors',
      \      'linter_ok': 'lightline#ale#ok',
      \ },
      \ 'component_type': {
      \     'linter_checking': 'left',
      \     'linter_warnings': 'warning',
      \     'linter_errors': 'error',
      \     'linter_ok': 'left',
      \ },
      \ 'active': {
      \     'right': [ [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_ok' ],
      \                [ 'percent',  'lineinfo'],
      \                [ 'fileformat', 'fileencoding', 'filetype'] ]
      \  },
      \ }

" ###########
" # Plugins #
" ###########

" Indent guides
"
hi IndentGuidesOdd  ctermbg=8
hi IndentGuidesEven ctermbg=8
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
let g:indent_guides_enable_on_vim_startup = 1

" ALE

let g:ale_completion_enabled = 0
let g:ale_linters_explicit = 1
let g:ale_linters = {
      \   'cpp': ['clangcheck', 'clangtidy', 'cppcheck'],
      \   'javascript': ['eslint'],
      \   'vim': ['vint'],
      \}

let b:ale_fixers = {
      \ 'javascript': ['eslint']
      \}


" CoC

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
" nmap <silent> <C-d> <Plug>(coc-range-select)
" xmap <silent> <C-d> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
